import exercise_4_helper
# Working with Functions

employees_list = [{
  "name": "Tina",
  "age": 30,
  "birthday": "1990-03-10",
  "job": "DevOps Engineer",
  "address": {
    "city": "New York",
    "country": "USA"
  }
},
{
  "name": "Tim",
  "age": 35,
  "birthday": "1985-02-21",
  "job": "Developer",
  "address": {
    "city": "Sydney",
    "country": "Australia"
  }
}]

if __name__ == '__main__':
    exercise_4_helper.part1(employees_list)

    string = "Counting all of The UpperCASE Letters" # 8 upper, 24 lower
    exercise_4_helper.part2(string)

    numbers_list = [1,2,3,4,5]
    exercise_4_helper.part3(numbers_list)