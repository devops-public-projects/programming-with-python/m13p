def part1(employees) -> None:
    '''
    Accept a list of dictionaries from input(),
    then print the youngest employees name and age.
    '''
    print("Part 1")
    youngest_index = 0

    for index in range(len(employees)):
        if employees[index]['age'] < employees[youngest_index]['age']:
            youngest_index = index

    print(f"Youngest Employee: {employees[youngest_index]['name']}, {employees[youngest_index]['age']}")
    print()

def part2(string_to_parse) -> None:
    '''
    Accept a string and calculate the upper and lower case numbers
    '''
    print("Part 2")

    u_count = 0
    l_count = 0
    
    for letter in string_to_parse:
        if letter.isupper():
            u_count += 1
        elif letter.islower():
            l_count += 1
    
    print(f'Upper Count: {u_count}')
    print(f'Lower Count: {l_count}')
    print()

def part3(numbers_list) -> None:
    '''
    Print the even numbers from a provided as a definition parameter.
    '''
    print("Part 3")

    print("Even Numbers: ")
    for x in numbers_list:
        if x % 2 == 0:
            print(x)