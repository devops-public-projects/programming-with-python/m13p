# List Comprehension

my_list = [1, 2, 2, 4, 4, 5, 6, 8, 10, 13, 22, 35, 52, 83]
found_list = []

def part1() -> None:
    '''
    Prints all numbers explicitly higher than 10 in the list.
    '''
    
    for x in my_list:
        if x > 10:
            print(x)

def part2() -> None:
    '''
    Saves elements higher than 10 into a new array, then prints.
    '''
    found_list = []

    for x in my_list:
        if x > 10:
            found_list.append(x)

    print(','.join([str(y) for y in found_list]))

def part3() -> None:
    '''
    Asks the user for an input number, then saves values that are higher
    to a new list and then prints that list.
    '''
    found_list = []

    number = int(input('Enter a number: ').strip())

    for x in my_list:
        if x > number:
            found_list.append(x)

    print(','.join([str(y) for y in found_list]))

if __name__ == '__main__':
    part1()
    part2()
    part3()