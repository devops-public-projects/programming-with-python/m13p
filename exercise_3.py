# List of two dictionaries

employees = [{
  "name": "Tina",
  "age": 30,
  "birthday": "1990-03-10",
  "job": "DevOps Engineer",
  "address": {
    "city": "New York",
    "country": "USA"
  }
},
{
  "name": "Tim",
  "age": 35,
  "birthday": "1985-02-21",
  "job": "Developer",
  "address": {
    "city": "Sydney",
    "country": "Australia"
  }
}]


def part1() -> None:
    '''
    Prints name, job, city of each employee in the list of dicionaries with a loop.
    '''

    for employee in employees:
        employee_info = []
        employee_info.append(employee['name'])
        employee_info.append(employee['job'])
        employee_info.append(employee['address']['city'])

        print(','.join(employee_info))


def part2() -> None:
    '''
    Prints the second employees country without a loop.
    '''
    print(employees[1]['address']['country'])

if __name__ == '__main__':
    part1()
    part2()