# Working with Dictionaries

employee = {
  "name": "Tim",
  "age": 30,
  "birthday": "1990-03-10",
  "job": "DevOps Engineer"
}

def part1() -> None:
    #Update Job Title
    employee['job'] = "Software Engineer"

    #Remove Age Key
    employee.pop('age')

    #Iterate and print all values
    for x in list(employee.keys()):
        print(f'{x}:{employee[x]}')

dict_one = {'a': 100, 'b': 400} 
dict_two = {'x': 300, 'y': 200}

def part2() -> None:
    #Merge dict_one and dict_two into dict_three
    dict_three = {}
    dict_three.update(dict_one)
    dict_three.update(dict_two)

    #Sum all values in dict_three
    sum = 0
    for x in dict_three.values():
        sum += x

    print(sum)

    #Print min and max values in dictionary
    minimum = min(dict_three.values())
    maximum = max(dict_three.values())

    print(f'Minimum: {minimum}')
    print(f'Minimum: {maximum}')
    

if __name__ == '__main__':
    part1()
    part2()