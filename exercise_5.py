import sys
# Calculator Program

calculation_count = 0

def get_number_input() -> list:
    continue_loop = True
    numbers = ''

    while continue_loop:
        numbers = input("Enter two numbers separated by a space: ").strip()
        
        if numbers == "exit":
            close_program()

        numbers_list = numbers.split(' ')
        
        if numbers_list[0].isnumeric and numbers_list[1].isnumeric:
            return numbers_list
        else:
            print("One of the values is not a number, try again")
        
def get_operator_input() -> str:
    while True:
        operator = input("Enter an operator: ").strip()

        if operator not in ['+','-','*','/','exit']:
            print("Invalid operator, try again")
            continue
        elif operator == 'exit':
            close_program()
        else:
            return(operator)

def close_program() -> None:
    print(f"Exiting Program, Total Calculations: {calculation_count}")
    sys.exit()

def main() -> None:
    global calculation_count

    while True:
        number_input = get_number_input()
        operator_input = get_operator_input()

        expression = f'{number_input[0]}{operator_input}{number_input[1]}'

        print(eval(expression))

        calculation_count += 1

if __name__ == '__main__':
    main()