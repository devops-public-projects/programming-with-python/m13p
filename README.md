
# Automation with Python Scripts
Bootcamp projects for Techworld with Nana Module 13 Premium.

## Technologies Used
- Python
- IntelliJ
- GitLab API
- 3rd Party Python Library **openpyxl**

## Project Descriptions

### Exercise 1 | Working with Lists
- With a given list perform these actions
	- Part 1 | Print elements larger than 10
	- Part 2 | Make a new list with all elements larger than 10 and print out the new list
	- Part 3 | Ask the user for a number, and print out elements larger than that number

### Exercise 2 | Working with Dictionaries
- With a given dictionary perform these actions
	- Part 1
		- Update the `job title`
		- Remove the `age` key
		- Loop through and print out `key:value` pairs
	- Part 2
		- Merge two dictionaries
		- Sum all values and print the result
		- Print min and max values

### Exercise 3 | Working with a List of Dictionaries
- With a given list of dictionaries perform these actions
	- Print the `name`, `job`, and `city`, of all employees
	- Print the `country` of the second employee in the list directly accessing it

### Exercise 4 | Working with Functions
- Write a function to accept a list of dictionaries
- Write a function to accept a string and calculate the upper and lower case letters
- Write a function to print even numbers from a provided list
- Move all code into a helper module to keep the main file clean

### Exercise 5 | Python Program "Calculator"
- Program Details
	- Take an input of 2 numbers
		- Re-prompt user if values are not integers
	- Take input of an operator (+ - * /)
	- Keep program running until the user types `exit`
	- Tally how many arithmetic operations the user entered and print on exit

### Exercise 6 | Python Program "Guessing Game"
- Generate a random number
- Ask the user to guess and notify if the guess is too high or low

### Exercise 7 | Working with Classes and Objects
- Create a Student, Professor, Lecture class
	- Extra Person class that Student and Professor inherit from
- Perform some tests on the Objects by calling their internal methods

### Exercise 8 | Working with Dates
- Accept a users birthday
- Calculate how long it is until their next birthday and print it out

### Exercise 9 | Working with Spreadsheets
- Open a spreadsheet with a 3rd Party Library `openpyxl`
- Parse `employee names` and `years of experience`
- Save to a new file the `employee names` and `years of experience` sorted by `years of experience` descending

### Exercise 10 | Working with REST APIs
- Connect to GitHub API
- Get all public repositories for a specific user
- Print all project names and URL's
