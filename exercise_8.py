from datetime import datetime
'''
This program will print out the time until your next birthday. If today is your birthday,
it will also print out 'Happy Birthday!'
'''

# Working with Dates

def main():
    users_birthday = get_users_birthday()

    today = datetime.today()
    year = determine_next_birthday_year(users_birthday, today)

    if users_birthday[1] == today.month and users_birthday[2] == today.day:
        print("Happy Birthday!")
    
    users_birthday[0] = year

    calculate_next_birthday(users_birthday, today)

def calculate_next_birthday(users_birthday: list, today: datetime):
    birthday = datetime(*users_birthday)

    delta = birthday - today

    hours_minutes = get_hours_minutes(delta)
    print(f"Your next birthday is in {delta.days} days, {hours_minutes[0]} hours, {hours_minutes[1]} minutes")

def determine_next_birthday_year(users_birthday: list, today: datetime) -> int:
    month = users_birthday[1]
    day = users_birthday[2]

    if month < today.month: #Birthday passed
        return today.year + 1
    
    elif month == today.month: #Birthday may have passed, check days

        if day < today.day: #Birthday has happened
            return today.year + 1

        elif day == today.day: #Birthday is Today, look for next one
            return today.year + 1

        else: #Birthday hasn't happened (> today.day)
            return today.year
        
    else: #Birthday hasn't happened in any circumstance
        return today.year

def get_hours_minutes(delta: datetime) -> list:
    '''
    Turns the timedelta into the hours and minutes from the seconds that are stored
    in the object.
    '''
    hours = delta.seconds // 3600
    minutes = (delta.seconds//60) % 60

    return [hours,minutes]

def get_users_birthday() -> list:
    '''
    Get the users birthday as Month/Day/Year and turn it into a list
    in format [year,month,day,hour,minute,second]
    '''

    user_input = list(map(int,input("Enter your birthday in Month/Day/Year Format: ").strip().split('/')))

    users_birthday = [user_input[2],user_input[0],user_input[1],0,0,0]

    return users_birthday

if __name__ == '__main__':
    main()