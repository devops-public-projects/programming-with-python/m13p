class Lecture:

    def __init__(self, name: str, max_students: int, duration: int, professors: list):
        self.name = name
        self.max_students = max_students
        self.duration = duration
        self.professors = professors

    def get_name_and_duration(self):
        print(f'{self.name} : {self.duration} minutes')

    def add_professor(self, professor: str):
        self.professors.append(professor)

    def get_professors(self):
        for professor in self.professors:
            professor.get_full_name()