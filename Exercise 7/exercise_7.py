from professor import Professor
from student import Student
from lecture import Lecture

# Classes and Objects

if __name__ == '__main__':
    
    # Add some Lectures
    comp_sci = Lecture("Computer Science I", 20, 90, [])
    physics = Lecture("Physics I", 50, 85, [])
    orgo_chem = Lecture("Organic Chemistry", 25, 75, [])
    algebra = Lecture("Algebra", 30, 80, [])

    # Add some Students
    student_1 = Student("John","Smith",22,[physics,comp_sci])
    student_2 = Student("Chris","Redfield",20,[algebra,orgo_chem])
    student_3 = Student("Jill","Valentine",21,[orgo_chem,physics,comp_sci])
    

    # Add some Professors
    professor_1 = Professor("Ada","Wong",34,[comp_sci,physics])
    professor_2 = Professor("Leon","Kennedy",34,[orgo_chem,algebra])

    #Test Methods
    print("-----Student Tests:-----")

    print("Get Full Name")
    student_1.get_full_name()
    
    print("\nAdd Lecture and Print")
    student_2.add_lecture(comp_sci)
    student_2.get_lectures()

    print("\nRemove Lecture and Print")
    student_3.remove_lecture(orgo_chem)
    student_3.get_lectures()


    print("\n\n-----Professor Tests:-----")

    print("Get Full Name")
    professor_1.get_full_name()

    print("\nAdd Lecture and Print")
    professor_2.add_subject(comp_sci)
    professor_2.get_subjects()

    print("\nRemove Lecture and Print")
    professor_1.remove_subject(comp_sci)
    professor_1.get_subjects()


    print("\n\n-----Lecture Information:-----")
    comp_sci.get_name_and_duration()
    comp_sci.add_professor(professor_1)
    comp_sci.get_professors()