from person import Person

class Student(Person):

    def __init__(self, first_name: str, last_name:str , age: int, lectures: list):
        super().__init__(first_name,last_name,age)
        self.lectures = lectures

    def get_lectures(self):
        print("Lectures: ")
        for lecture in self.lectures:
            print(lecture.name)

    def add_lecture(self, lecture_name):
        self.lectures.append(lecture_name)

    def remove_lecture(self, lecture_name):
        self.lectures.remove(lecture_name)