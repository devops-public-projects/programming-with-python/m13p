from person import Person

class Professor(Person):
    def __init__(self, first_name: str, last_name:str , age: int, subjects: list):
        super().__init__(first_name,last_name,age)
        self.subjects = subjects

    def get_subjects(self):
        print("Lectures: ")
        for subject in self.subjects:
            print(subject.name)

    def add_subject(self, subject_name):
        self.subjects.append(subject_name)

    def remove_subject(self, subject_name):
        self.subjects.remove(subject_name)   