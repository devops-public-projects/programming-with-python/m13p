import requests

# GitHub API

username = ""

def main():
    '''
    Program will list all GitHub projects of a specific user
    '''

    response = requests.get(f'https://api.github.com/users/{username}/repos')

    my_projects = response.json()

    for project in my_projects:
        print(f'Project Name: {project["name"]}, Project Url: {project["html_url"]}')

if __name__ == '__main__':
    main()