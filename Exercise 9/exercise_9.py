import openpyxl
# Spreadsheets

def main():
    '''
    Open the file, take the name and years of experience, sort it all and then
    add it to a new spreadsheet and save
    Assumptions: no one has the same name.
    '''
    employee_file = openpyxl.load_workbook(r'''employees.xlsx''')
    employee_list = employee_file['Sheet1']

    employees = {}

    # Get all Employees and Years of Experience in a data structure
    for employee_row in range(2, employee_list.max_row + 1):
        employee_name = employee_list.cell(employee_row, 1).value
        employee_exp = employee_list.cell(employee_row, 2).value

        if employee_exp not in employees:
            employees[employee_exp] = []

        employees[employee_exp].append(employee_name)

    #Remove the Job Title and DoB Columns
    employee_list.delete_cols(3,2)

    # Sort by Years of Experience and Save
    dict_keys = employees.keys()
    keys_list = list(dict_keys)
    keys_list.sort(reverse=True)

    row_index = 2 # After the header, our data begins

    for key in keys_list:
        employee_names = employees[key]
        employee_names.sort() # Sort employee names alpabetically

        for employee_name in employee_names:
            employee_list.cell(row_index,1).value = employee_name
            employee_list.cell(row_index,2).value = key
            row_index += 1

    employee_file.save(r'''employees_sorted.xlsx''')

if __name__ == '__main__':
    main()