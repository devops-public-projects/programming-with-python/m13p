import random
# Guessing Game

def main() -> None:
    random_number = random.randint(1,9)
    
    while True:
        user_guess = int(input('Guess a Number: ').strip())

        if user_guess < random_number:
            print("Your guess is too low!")
        elif user_guess > random_number:
            print("Your guess is too high!")
        elif user_guess == random_number:
            print("YOU WON!")
            break


if __name__ == '__main__':
    main()